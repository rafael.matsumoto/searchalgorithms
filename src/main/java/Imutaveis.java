public class Imutaveis {
    public static void main(String[] args){
        String palavraImutavelUm = new String("Olá");

        String palavraImutavelDois = "Olá";

        System.out.println(palavraImutavelUm);
    }
}
