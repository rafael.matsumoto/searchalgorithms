import java.util.ArrayList;

import static org.junit.Assert.*;

public class KMPTest {

    @org.junit.Test
    public void searchOneFound() {
        KMP kmpSearch = new KMP("brown");
        assertEquals(10, kmpSearch.search("The quick brown fox jumps over the lazy dog"));
    }

    @org.junit.Test
    public void searchNoneFound(){
        KMP kmpSearch = new KMP("FAA");
        assertEquals(11, kmpSearch.search("AABCCAADDEE"));
    }

}