import static org.junit.Assert.*;

public class BoyerMooreTest {

    @org.junit.Test
    public void searchOneFound() {
        BoyerMoore boyerMooreSearch = new BoyerMoore("brown");
        assertEquals(10, boyerMooreSearch.search("The quick brown fox jumps over the lazy dog"));
    }

    @org.junit.Test
    public void searchNoneFound(){
        BoyerMoore  boyerMooreSearch = new BoyerMoore("FAA");
        assertEquals(11, boyerMooreSearch.search("AABCCAADDEE"));
    }

}