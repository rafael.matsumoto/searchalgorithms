import org.junit.Before;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class NaiveSearchTest {

    @org.junit.Test
    public void searchOneFound() {
        NaiveSearch searchAlgorithm = new NaiveSearch("The quick brown fox jumps over the lazy dog", "brown");
        ArrayList<Integer> expectedResult = new ArrayList<>();
        expectedResult.add(10);
        assertEquals(expectedResult, searchAlgorithm.search());
    }

    @org.junit.Test
    public void searchMultipleFound(){
        NaiveSearch searchAlgorithm = new NaiveSearch("AABAACAADAABAABA", "AABA");
        ArrayList<Integer> expectedResult = new ArrayList<>();
        expectedResult.add(0);
        expectedResult.add(9);
        assertEquals(expectedResult, searchAlgorithm.search());
    }

    @org.junit.Test
    public void searchNoneFound(){
        NaiveSearch searchAlgorithm = new NaiveSearch("AABCCAADDEE","FAA");
        ArrayList<Integer> expectedResult = new ArrayList<>();
        assertEquals(expectedResult, searchAlgorithm.search());
    }
}