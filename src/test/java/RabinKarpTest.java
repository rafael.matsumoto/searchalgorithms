import static org.junit.Assert.*;

public class RabinKarpTest {

        @org.junit.Test
        public void searchOneFound() {
            RabinKarp rabinKarpSearch = new RabinKarp("brown");
            assertEquals(10, rabinKarpSearch.search("The quick brown fox jumps over the lazy dog"));
        }

        @org.junit.Test
        public void searchNoneFound(){
            RabinKarp rabinKarpSearch = new RabinKarp("FAA");
            assertEquals(11, rabinKarpSearch.search("AABCCAADDEE"));
        }

}